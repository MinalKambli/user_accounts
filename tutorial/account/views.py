from django.shortcuts import render,redirect
from django.core.mail import send_mail
from django.conf import settings
from rest_framework import viewsets
from .models import MyUser
from .serializers import MyUserSerializer
from account.forms import (
	RegistrationForm, 
	EditProfileForm
	)
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserChangeForm, PasswordChangeForm
from django.contrib.auth import update_session_auth_hash

class MyUserViewSet(viewsets.ModelViewSet):
	queryset = MyUser.objects.all()
	serializer_class = MyUserSerializer

def home(request):
	return render(request, 'accounts/home.html')

def register(request):
	if request.method == 'POST':
		form = RegistrationForm(request.POST)
		if form.is_valid():
			form.save()
			print(form.cleaned_data['email'], 'form')
			user_mail = form.cleaned_data['email']
			to_mail = [user_mail, settings.EMAIL_HOST_USER]
			from_email = settings.EMAIL_HOST_USER
			email_subject = 'Test mail'
			email_body = 'This is test mail'
			send_mail(email_subject, email_body, from_email, to_mail, fail_silently=True)
			return redirect('/account')
	else:
		form = RegistrationForm()
	args = {'form': form}
	return render(request, 'accounts/reg_form.html', args)

def view_profile(request):
	args = {'user': request.user}
	return render(request, 'accounts/profile.html', args)

def edit_profile(request):
	if request.method == 'POST':
		form = EditProfileForm(request.POST, instance=request.user)
		if form.is_valid():
			form.save()
			return redirect('/account/profile')
	else:
		form = EditProfileForm(instance=request.user)
		args = {'form': form}
	return render(request, 'accounts/edit_profile.html', args)

def change_password(request):
	if request.method == 'POST':
		form = PasswordChangeForm(data=request.POST, user=request.user)
		if form.is_valid():
			form.save()
			update_session_auth_hash(request, user=form.user)
			return redirect('/account/profile')
		else:
			return redirect('/account/change-password')
	else:
		form = PasswordChangeForm(user=request.user)
	args = {'form': form}
	return render(request, 'accounts/change_password.html', args)