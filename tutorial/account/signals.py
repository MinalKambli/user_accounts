from django.contrib.auth.models import User
from django.db.models import signals
from django.dispatch import receiver
from django.db.models.signals import post_save, m2m_changed
from account.models import MyUser
from account.tasks import *

@receiver(post_save, sender=MyUser)
def send_notifications_user_registration(sender, instance, **kwargs):
    return user_sign_up(sender, instance, **kwargs)