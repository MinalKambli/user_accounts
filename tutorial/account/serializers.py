from django.contrib.auth.models import User
from .models import MyUser
from rest_framework import serializers
from rest_framework.validators import UniqueValidator

class MyUserSerializer(serializers.ModelSerializer):
	class Meta:
		model = MyUser
		fields = (
			'id',
			'user',
			'alternate_email',
			'image'
			)