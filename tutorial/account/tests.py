from django.core.urlresolvers import reverse
from rest_framework.test import APITestCase
from django.contrib.auth.models import User
from rest_framework import status
from .models import MyUser

class AccountsTest(APITestCase):
    def setUp(self):
        self.test_user = MyUser.objects.create_user('admin', 'test@example.com', '')

        self.create_url = reverse('account-create')

    def test_create_user(self):
        data = {
            'user': 'admin',
            'alternate_email': 'foobar@example.com',
            'image': ''
        }

        response = self.client.post(self.create_url , data, format='json')
        user = MyUser.objects.latest('id')
        token = Token.objects.get(user=user)
        self.assertEqual(MyUser.objects.count(), 2)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(response.data['user'], data['user'])
        self.assertEqual(response.data['alternate_email'], data['alternate_email'])
        self.assertEqual(response.data['image'], data['image'])