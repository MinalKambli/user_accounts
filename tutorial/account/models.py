from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save
# Create your models here.
class MyUser(models.Model):
	user = models.OneToOneField(User)
	alternate_email = models.EmailField()
	image = models.ImageField(upload_to='profile_image', blank=True)

def create_profile(sender, **kwargs):
	if kwargs['created']:
		my_user = MyUser.objects.create(user=kwargs['instance'])
post_save.connect(create_profile, sender=User)
