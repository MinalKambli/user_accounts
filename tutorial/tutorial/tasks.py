from __future__ import absolute_import, unicode_literals
from celery.decorators import task
from .celery import app
from django.core.mail import send_mail
import requests
from django.conf import settings

@app.task
def send_mail_on_registration(subject, body, sender, receiver):
	return send_mail(subject, body , sender , receiver, fail_silently=False)